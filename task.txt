Story 1:
1. 编写staff对象。
2. 编写StaffRepository，实现staff的save。
3. 在staffService中编写save方法，实现staff的保存。
4. 在StaffController中编写save方法，实现staff的保存。
5. 在StaffRepository实现，可以根据ID查询Staff。
6. 在StaffController中编写find方法，根据给定的id查找staff。
7. 在StaffRepository实现，查找所有staff。
8. 在StaffController中实现，查找所有staff。

Story 2:
1 更改staff的表结构。
2 更改staff对象的信息。
3 在staffController中实现给staff添加时区信息。
4 获取staff信息时，当staff拥有timeZone应该返回zoneId。
5 获取staff信息时，当staff没有timeZone应该返回null。


Story 3:
1 在TimeZoneController中，实现查询timeZones的功能。


Story 4:
1 创建reservations表。
2 创建reservations对象，满足约束条件。
3 创建ReservationRepository,实现reservations的保存。
4 在ReservationController中，实现reservations的保存。
5 在ReservationController中，实现reservations的查询。



