package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findAllByRodIdOrderByStartTime(Long rodId);
}
