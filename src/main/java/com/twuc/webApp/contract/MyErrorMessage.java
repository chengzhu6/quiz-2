package com.twuc.webApp.contract;

public class MyErrorMessage {

    private String message;


    public MyErrorMessage() {

    }

    public MyErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
