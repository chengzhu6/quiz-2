package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StaffInfoRequest {


    @Size(max = 64)
    @NotNull
    private String firstName;

    @Size(max = 64)
    @NotNull
    private String lastName;

    public StaffInfoRequest() {

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
