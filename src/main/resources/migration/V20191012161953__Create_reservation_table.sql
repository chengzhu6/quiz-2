create table `reservation` (
    id  BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_name varchar(128) NOT NULL,
    company_name varchar(64) NOT NULL,
    zone_id varchar(30) NOT NULL,
    start_time date NOT NULL,
    duration varchar(4) NOT NULL,
    rob_id BIGINT NOT NULL
);
