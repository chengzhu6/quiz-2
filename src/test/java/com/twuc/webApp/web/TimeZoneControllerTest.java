package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class TimeZoneControllerTest extends ApiTestBase {


    @Test
    void should_get_time_zones() throws Exception {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> sorted = availableZoneIds.stream().sorted().collect(Collectors.toList());
        String zoneIdsJson = serialize(sorted);

        mockMvc.perform(get("/api/timezones"))
                .andExpect(MockMvcResultMatchers.content().string(zoneIdsJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }



}
