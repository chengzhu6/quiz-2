package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.ReservationResponse;
import com.twuc.webApp.entity.Reservation;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

class StaffControllerTest extends ApiTestBase {


    @Autowired
    StaffRepository staffRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_staff_when_given_staff() throws Exception {
        String requestJson = "{\"firstName\": \"Rob\",  \"lastName\": \"Hall\"}";
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
                        .andExpect(MockMvcResultMatchers.status().is(201))
                        .andExpect(MockMvcResultMatchers.header().string("location", "/api/staffs/1"));
    }


    @Test
    void should_get_400_staff_when_given_staff_name_is_valid() throws Exception {
        String requestJson = "{\"firstName\": \"asdfadsfadfasdfaRobdadfasdfasdfasdfasdfasdfasdfasdfadsffasdfffaads\",  \"lastName\": \"Hall\"}";
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_get_staff_info_when_given_a_staff_id() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        String oldStaffJson = serialize(oldStaff);
        mockMvc.perform(get(String.format("/api/staffs/%s", oldStaff.getId())))
                .andExpect(MockMvcResultMatchers.content().string(oldStaffJson));
    }

    @Test
    void should_return_404_when_staff_is_not_exist() throws Exception {
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_get_staffs() throws Exception {
        Staff firstStaff = staffRepository.save(new Staff("zhang", "san"));
        Staff secondStaff = staffRepository.save(new Staff("zhang", "si"));
        String staffsJson = serialize(Arrays.asList(firstStaff, secondStaff));
        mockMvc.perform(get("/api/staffs"))
                .andExpect(MockMvcResultMatchers.content().string(staffsJson));
    }


    @Test
    void should_get_none_when_none_staff() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }


    @Test
    void should_update_staff_zone_when_given_a_staff_id_and_zone_id() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));
        Staff updatedStaff = staffRepository.findById(oldStaff.getId()).orElse(null);
        assert updatedStaff != null;
        assertEquals("Asia/Chongqing", updatedStaff.getZoneId());
    }

    @Test
    void should_return_400_when_given_a_staff_id_and_valid_zone_id() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqin\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_get_time_zone_info_when_get_staff_info() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));

        mockMvc.perform(get(String.format("/api/staffs/%s", oldStaff.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.zoneId").value("Asia/Chongqing"));

    }

    @Test
    void given_staff_without_time_zone_should_get_time_zone_is_null_when_get_staff_info() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();

        mockMvc.perform(get(String.format("/api/staffs/%s", oldStaff.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.zoneId").isEmpty());

    }


    @Test
    void should_save_reservation_when_give_a_reservation_to_a_rob() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%s/reservations", oldStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(MockMvcResultMatchers.status().is(201))
        .andExpect(MockMvcResultMatchers.header().string("location", String.format("/api/staffs/%s/reservations", oldStaff.getId())));

        List<Reservation> allReservations = reservationRepository.findAll();
        assertEquals(1, allReservations.size());
        Staff rod = allReservations.get(0).getRod();
        assertNotNull(rod);
        assertEquals(oldStaff.getFirstName(), rod.getFirstName());
    }


    @Test
    void should_return_409_when_give_a_reservation_to_a_rob() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-13T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%s/reservations", oldStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(MockMvcResultMatchers.status().is(409));
    }

    @Test
    void should_get_all_reservations_belong_to_staff() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));

        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%s/reservations", oldStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("location", String.format("/api/staffs/%s/reservations", oldStaff.getId())));

        List<Reservation> allReservations = reservationRepository.findAllByRodIdOrderByStartTime(oldStaff.getId());
        List<Object> reservationResponses = allReservations.stream()
                .map(item -> ReservationResponse.createReservationResponse(item, oldStaff))
                .collect(Collectors.toList());
        String allReservationsJson = serialize(reservationResponses);
        System.out.println(allReservationsJson);
        mockMvc.perform(get(String.format("/api/staffs/%s/reservations", oldStaff.getId())))
                .andExpect(MockMvcResultMatchers.content().string(allReservationsJson));
    }
}
