package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class StaffRepositoryTest {


    @Autowired
    StaffRepository staffRepository;

    @Autowired
    EntityManager em;

    @Test
    void should_save_staff_to_db() {

        Staff staff = new Staff("zhang", "san");
        staffRepository.save(staff);
        em.flush();
        em.clear();
        Staff staffFound = staffRepository.findById(staff.getId()).orElse(null);
        assert staffFound != null;
        assertEquals("zhang", staffFound.getFirstName());
        assertEquals("san", staffFound.getLastName());
    }
}
